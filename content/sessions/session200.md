---
title: "نشست ۲۰۰"
author: "بهزادی"
date: "1399-12-20"
description: "معرفی زبان گو (an introduction to GO)"
subtitle: "معرفی زبان گو (an introduction to GO)"
weight: -1
summaryImage: "/img/sessions/poster200.jpg"
tags: []
images: []
readmore: false
draft: false
---
[![معرفی زبان گو (an introduction to GO)](/img/sessions/poster200.jpg)](/img/sessions/poster200.jpg)
